#!/bin/bash

cronfile=/etc/crontabs/root
cat /dev/null > $cronfile
for cronvar in ${!CRON_*}; do
	cronvalue=${!cronvar}
	echo "Installing $cronvar"
	echo "$cronvalue" >> $cronfile
done
echo >> $cronfile # Newline is required

chmod 0644 "${cronfile}"

/usr/sbin/crond -f