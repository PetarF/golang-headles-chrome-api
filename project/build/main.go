package main

import (
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"
)

var exposedPort string
var allwedParams = []string{"width", "height", "no-margin", "hide-scrollbars"} //,"dump-dom"
var mode = "--print-to-pdf"
var ext = "pdf"
var folder = "/tmp/app_files"

type urlType struct {
	Url string
}
type jsonResponse struct {
	Data urlType
}

func init() {
	exposedPort = ""
	if s, ok := os.LookupEnv("CA_PORT"); ok {
		exposedPort = s
	} else {
		log.Fatalf("Exposed Port is not defined or empty")
	}
	
	if f, ok := os.LookupEnv("CA_FOLDER"); ok {
		folder = f
	}
}

/**
*pdf route
*@Get("url") url for proccessing
**/
func pdf(w http.ResponseWriter, r *http.Request) {
	checkRequiredArgs(w,r)
	mode = "--print-to-pdf"
	ext = "pdf"

	createJSONResponse(r.URL.Query().Get("url"), w, r)
	arguments := createChromeAppArguments(r)

	runHeadlessChrome(arguments)
}

/**
*screenshot route
*@Get("url") url for proccessing
**/
func screenshot(w http.ResponseWriter, r *http.Request) {
	checkRequiredArgs(w,r)
	mode = "--screenshot"
	ext = "png"
	createJSONResponse(r.URL.Query().Get("url"), w, r)
	arguments := createChromeAppArguments(r)

	runHeadlessChrome(arguments)
}

func checkRequiredArgs(w http.ResponseWriter,r *http.Request) {
	param := r.URL.Query().Get("url")
	if param == "" {
		http.Error(w, "Get 'url' not specified in url.", 500)
		return
	}
}

func createJSONResponse(param string, w http.ResponseWriter, r *http.Request) {
	response := jsonResponse{
		Data: urlType{
			Url: "http://" + r.Host + "/api/download?file=" + getFileName(param),
		},
	}
	encoded, _ := json.Marshal(response)
	w.Write(encoded)
}

func createChromeAppArguments(r *http.Request) []string {
	file := fmt.Sprint(mode, "=", folder, "/", getFileName(r.URL.Query().Get("url")))
	chromeappArg := []string{file , "--enable-viewport", "--headless", "--no-sandbox", "--disable-gpu", "--run-all-compositor-stages-before-draw", r.URL.Query().Get("url")}
	windowSize := ""
	width := ""
	height := ""

	for _, val := range allwedParams {

		if v := r.URL.Query().Get(val); v != "" {

			if val == "width" {
				width = v
			} else if val == "height" {
				height = v
			} else {
				chromeappArg = append([]string{"--" + val}, chromeappArg...)
			}

		}
	}

	if width != "" && height != "" {
		windowSize = "--window-size=" + width + "," + height
	}

	if windowSize != "" {
		chromeappArg = append([]string{windowSize}, chromeappArg...)
	}
	fmt.Println(chromeappArg)
	return chromeappArg
}

/**
*Route download
*@Get("file") file to download
*@Get("display") outputs file to browser, downloads by default
**/
func download(w http.ResponseWriter, r *http.Request) {

	d := "attachment"
	param := r.URL.Query().Get("file")
	if param == "" {
		http.Error(w, "Get 'file' not specified in url.", 500)
		return
	}
	display := r.URL.Query().Get("display")
	if display != "" {
		d = "inline"
	}

	file := folder+"/" + param
	// Open file
	f, err := os.Open(file)
	if err != nil {
		fmt.Println(err)
		w.WriteHeader(500)
		return
	}
	defer f.Close()

	//Get the Content-Type of the file
	//Create a buffer to store the header of the file in
	fileHeader := make([]byte, 512)
	//Copy the headers into the FileHeader buffer
	f.Read(fileHeader)
	//Get content type of file
	fileContentType := http.DetectContentType(fileHeader)

	//Get the file size
	fileStat, _ := f.Stat()                            //Get info from file
	fileSize := strconv.FormatInt(fileStat.Size(), 10) //Get file size as a string

	//Set header
	w.Header().Set("Content-Disposition", d+"; filename="+param)
	w.Header().Set("Content-Type", fileContentType)
	w.Header().Set("Content-Length", fileSize)

	f.Seek(0, 0)

	//Stream to response
	if _, err := io.Copy(w, f); err != nil {
		fmt.Println(err)
		w.WriteHeader(500)
	}

}

func runHeadlessChrome(params []string) {
	cmd := exec.Command("chromium-browser", params...)
	err := cmd.Start()
	if err != nil {
		log.Println("cannot start browser", err)
	}
	err = cmd.Wait()
}

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/api/pdf", pdf)
	mux.HandleFunc("/api/screenshot", screenshot)
	mux.HandleFunc("/api/download", download)
	
	_=os.MkdirAll(folder, os.ModePerm)
	
	fmt.Println("Available routes:")
	fmt.Println("/api/screenshot")
	fmt.Println("/api/pdf")
	
	if err := http.ListenAndServe(":"+exposedPort, ipRestrictionMiddelware(mux)); err != nil {
		log.Fatalf("unable to start server: %s", err.Error())
	}
}

func ipRestrictionMiddelware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		if s, ok := os.LookupEnv("CA_ALLOWED_IP"); ok {
		
			if len(s) > 0 {
			
				ip := getRealAddr(r)
				allowedList := s

				if ok := strings.Contains(allowedList, ip); !ok {
					http.Error(w, http.StatusText(http.StatusForbidden), http.StatusForbidden)
					return
				}
			}
			
		}

		next.ServeHTTP(w, r)
	})
}

func getRealAddr(r *http.Request) string {

	remoteIP := ""
	// the default is the originating ip. but we try to find better options because this is almost
	// never the right IP
	if parts := strings.Split(r.RemoteAddr, ":"); len(parts) == 2 {
		remoteIP = parts[0]
	}
	// If we have a forwarded-for header, take the address from there
	if xff := strings.Trim(r.Header.Get("X-Forwarded-For"), ","); len(xff) > 0 {
		addrs := strings.Split(xff, ",")
		lastFwd := addrs[len(addrs)-1]
		if ip := net.ParseIP(lastFwd); ip != nil {
			remoteIP = ip.String()
		}
		// parse X-Real-Ip header
	} else if xri := r.Header.Get("X-Real-Ip"); len(xri) > 0 {
		if ip := net.ParseIP(xri); ip != nil {
			remoteIP = ip.String()
		}
	}

	return remoteIP

}

func getMD5Hash(text string) string {
	hasher := md5.New()
	hasher.Write([]byte(text))
	return hex.EncodeToString(hasher.Sum(nil))
}

func getFileName(base string) string {
	return fmt.Sprint(getMD5Hash(base), "_", time.Now().Unix(), ".", ext)
}
