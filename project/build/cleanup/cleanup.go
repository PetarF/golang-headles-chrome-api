package main

import (
    "fmt"
    "io/ioutil"
    "log"
    "time"
    "os"
	"strconv"
)

var cutoff = 60*5 * time.Minute

var folder = "/tmp/app_files"


func init() {
	
	if f, ok := os.LookupEnv("CA_FOLDER"); ok {
		folder = f
	}
	
	if t, ok := os.LookupEnv("CA_CLEANUP"); ok {
		
		v,_ := strconv.Atoi(t)
		
		cutoff = time.Duration(v) * time.Minute
	}else{
		fmt.Printf("%s","No ENV CA_CLEANUP, default time used\n")
	}
}

func main() {
	now := time.Now()
    fmt.Printf("%s","Cleaning files...\n")
    fmt.Printf("Current time: %s\n",now)
    fmt.Printf("Delete files older than %s located at %s\n",cutoff,folder)
	
    fileInfo, err := ioutil.ReadDir(folder)
    if err != nil {
        log.Fatal(err.Error())
    }
    
    for _, info := range fileInfo {
		
		fmt.Printf("diff: %s, cutoff: %s",now.Sub(info.ModTime()),cutoff)
		
        if diff := now.Sub(info.ModTime()); diff > cutoff {
		  if err := os.Remove(folder+"/"+info.Name()); err==nil {
                fmt.Printf("Deleting %s which is %s old\n", folder+info.Name(), diff)
           }else{
			fmt.Printf("%s", err)
		   }
        }
    }
}