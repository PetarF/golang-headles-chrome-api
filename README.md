API for creating Pdf from print Css written in Go.
---
It uses Headless Chrome to fetch url and generate pdf.
Returns JSON object with url to generated pdf.

This app consists of 2 containers:
1. The main app
2. The cleanup app 

Cleanup is done using https://github.com/PetarFedorovsky/docker-alpine-cron


---
Main app ENV variables
======

| ENV             | Description                                                    | Required  |
| --------------- |:--------------------------------------------------------------:| ---------:|
| CA_PORT         | inner port go app listens to, must be the same as mapped port. | yes       |
| CA_FOLDER       | folder to store files - defaults to /tmp/app_files             | no        |
| CA_ALLOWED_IP   | csv list of IP's that are allowed to access main app           | no        |


---
Cleanup ENV variables
======

| ENV             | Description                                                       | Required                       |
| --------------- |:-----------------------------------------------------------------:| ------------------------------:|
| CA_CLEANUP      | int minutes how old files must be to get deleted - defaults to 5h | no                             | 
| CA_FOLDER       | folder to store files - defaults to /tmp/app_files                | no /yes if changed im main app |


---
Available parameters
======

| Argument        | Method        | Endpoint  |
| --------------- |:-------------:| ---------:|
| hide-scrollbars | get           | screenshot,pdf |
| no-margin       | get           | screenshot,pdf |
| height          | get           | screenshot,pdf |
| width           | get           | screenshot,pdf |



Example:

Request:

http://localhost:8080/api/pdf?url=https://google.com

Response:

{"Data":{"Url":"http://localhost:8080/api/download?file=99999ebcfdb78df077ad2727fd00969f_1589993347.pdf"}}


